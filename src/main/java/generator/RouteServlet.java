/*
 * Copyright (C) 2011 The Libphonenumber Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author Shaopeng Jia
 */

package generator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberToCarrierMapper;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberType;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.PhoneNumberUtil.ValidationResult;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A servlet that accepts requests that contain strings representing a phone
 * number and a default country, and responds with results from parsing,
 * validating and formatting the number. The default country is a two-letter
 * region code representing the country that we are expecting the number to be
 * from.
 */
@SuppressWarnings("serial")
public class RouteServlet extends HttpServlet {

    private static final PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
    private static final Logger log = LoggerFactory.getLogger(RouteServlet.class);

    private static final String[] prefixes = { "1","7","20","27","30","31","32","33","34","36","39","40","41","43","44","45","46","47","48","49","51","52","53","54","55","56","57","58","60","61","62","63","64","65","66","77","81","82","84","86","90","91","92","93","94","95","98","211","212","213","216","218","220","221","222","223","224","225","226","227","228","229","230","231","232","233","234","235","236","237","238","239","240","241","242","243","244","245","246","247","248","249","250","251","252","253","254","255","256","257","258","260","261","262","263","264","265","266","267","268","269","290","291","297","298","299","350","351","352","353","354","355","356","357","358","359","370","371","372","373","374","375","376","377","378","380","381","382","383","385","386","387","389","420","421","423","500","501","502","503","504","505","506","507","508","509","590","591","592","593","594","595","596","597","598","599","670","672","673","674","675","676","677","678","679","680","681","682","683","685","686","687","688","689","690","691","692","800","808","850","852","853","855","856","870","878","880","881","882","883","886","888","960","961","962","963","964","965","966","967","968","970","971","972","973","974","975","976","977","979","992","993","994","995","996","998","1242","1246","1264","1268","1284","1340","1441","1473","1649","1664","1670","1671","1684","1758","1767","1784","1868","1869","1876"};
    private static final String[] usSpecial = { "1242", "1246", "1264", "1268", "1284", "1340", "1345", "1441", "1473", "1649","1664", "1658", "1670", "1671", "1684", "1721", "1758", "1767", "1784", "1787", "1809", "1829", "1849","1849", "1868", "1869", "1876", "1939" };
    private static final String[] kazakhstan = { "77" };

    public void writeCSV(TreeMap<String, RouteInfo> routes, OutputStream os) throws IOException {

        for (Map.Entry<String, RouteInfo> entry : routes.entrySet()) {
            RouteInfo info = entry.getValue();
            os.write(info.toString().getBytes());
            os.write("\n".getBytes());
        }
        os.flush();
        os.close();
    }

    public TreeMap<String, RouteInfo> buildRoutes(String[] _prefixes, String _maxPrefixLength, String _includeInvalidNumbers) throws InterruptedException, ExecutionException {

        TreeMap<String, RouteInfo> allRoutes = new TreeMap<String, RouteInfo>();

        //phoneUtil.getSupportedCallingCodes()
        //Arrays.stream(line.split(",")).mapto;

        String [] prefixes2Use = _prefixes != null ? _prefixes : prefixes;
        int maxPrefixLength = _maxPrefixLength != null ? Integer.parseInt(_maxPrefixLength) : 7;
        boolean includeInvalidNumbers = _includeInvalidNumbers != null ? Boolean.parseBoolean(_includeInvalidNumbers) : false;

        ExecutorService threadPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        ArrayList<Future> threads = new ArrayList<Future>();

        for (int i = 0; i < prefixes2Use.length; i++) {
            String prefix = prefixes2Use[i];
            String[] skip = {};
            if (prefix.startsWith("1")) {
                skip = Arrays.stream(usSpecial).filter(v ->!v.equalsIgnoreCase(prefix)).toArray(String[]::new);
            } else if (prefix.equalsIgnoreCase("7")) {
                skip = kazakhstan;
            }
            CountryThread t = new CountryThread(prefix, skip, maxPrefixLength, includeInvalidNumbers);
            threads.add(threadPool.submit(t));
        }

        // Wait for completion
        Iterator<Future> threadItr = threads.iterator();
        while (threadItr.hasNext()) {
            Future f = threadItr.next();
            TreeMap<String, RouteInfo> routes = (TreeMap<String, RouteInfo>) f.get();
            allRoutes.putAll(routes);
        }
        threadPool.shutdown();
        return allRoutes;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        Instant start = Instant.now();

        String includeInvalidNumbers = request.getParameter("invalid");
        String maxPrefixLength = request.getParameter("plen");
        String[] prefixes = request.getParameterValues("p");

        byte[] csv;
        try {
            TreeMap<String, RouteInfo> routes = buildRoutes(prefixes, maxPrefixLength, includeInvalidNumbers);
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            writeCSV(routes, os);
            csv = os.toByteArray();
        } catch(Exception e) {
            log.warn(e.toString());
            response.sendError(500);
            return;
        }

        response.setContentType("text/csv");
        response.setHeader("Content-Disposition", "attachment; filename=routes.csv");
        response.setContentLength(csv.length);
        ServletOutputStream out = response.getOutputStream();
        out.write(csv);
        out.flush();
        out.close();

        Instant end = Instant.now();
        log.info("completed (" + Duration.between(start, end) + ")");
    }

    private class RouteInfo implements Serializable, Comparable<RouteInfo> {
        
        private String shortTag;
        private String longTag;
        private ArrayList<String> prefixes = new ArrayList<String>();
        private String exampleNumber;
        private long cntNumbers = 0;
        private long cntNumbersTotal = 0;

        public RouteInfo(String longTag) {
            
            this.longTag = longTag;
            String[] splits = longTag.split("_");
            this.shortTag = splits[0];
            if (splits.length > 1) {
                this.shortTag += "_" + splits[1];
            }
        }

        public boolean isValid() {
            return !this.shortTag.startsWith("invalid");
        }

        public void addPrefix(String prefix) {
            if (!prefixes.contains(prefix)) {
                prefixes.add(prefix);
            }
        }

        public void clearPrefixes() {
            this.prefixes.clear();
        }

        public void merge(RouteInfo route) {
            if (!this.longTag.equalsIgnoreCase(route.longTag)) {
                RouteServlet.log.error("tags not matching: " + this.longTag + " != " + route.longTag);
                return;
            }
            route.prefixes.forEach(p -> this.addPrefix(p));
        }

        public String toString() {
            StringBuilder sb=new StringBuilder();
            sb.append(shortTag)
            .append(";")
            .append(longTag)
            .append(";")
            .append(prefixes.stream().collect(Collectors.joining(",")))
            .append(";")
            .append(exampleNumber)
            .append(";")
            .append(cntNumbers);
            if (cntNumbersTotal > 0) {
                sb.append(";")
                .append(String.format("%.05f", (float) cntNumbers/cntNumbersTotal));
            }
            return sb.toString();
        }

        public int compareTo(RouteInfo o) {
            return o.prefixes.size() - this.prefixes.size();
        }
    }

    final private static String generatePadding(int n) {
        String padding = "";
        int nextDigit = 5;
        while (n>0){
            padding += nextDigit;
            nextDigit = (nextDigit+3)%10;
            n--;
        }
        return padding;
    }

    final static Random random = new Random();
    final private static int generateRandomNumber() {
        return random.nextInt(10000000);
        //int m = (int) Math.pow(10, n - 1);
        //return String.valueOf(m + random.nextInt(9 * m));
    }

    final private static String camelCase(String text) {
        String bactrianCamel = Stream.of(text.split("[^a-zA-Z0-9]")).filter(v -> v.length() > 0)
                .map(v -> v.substring(0, 1).toUpperCase() + v.substring(1).toLowerCase())
                .collect(Collectors.joining());
        String cc = bactrianCamel.toLowerCase().substring(0, 1) + bactrianCamel.substring(1);
        return cc;
    }

    public class CountryThread implements Callable<TreeMap<String, RouteInfo>> {

        private String countryPrefix;
        private String[] skipPrefixes;
        private int maxPrefixLength = 7;
        private boolean includeInvalidNumbers = false;
        private TreeMap<String, RouteInfo> routes = new TreeMap<String, RouteInfo>();
        private HashMap<String, ArrayList<Integer>> randomValues = new HashMap<String, ArrayList<Integer>>();
        private HashMap<Integer, String> exampleNumbers = new HashMap<Integer, String>();
        private HashMap<String, AtomicInteger> cntPerRoute = new HashMap<String, AtomicInteger>();
        private HashMap<String, String> mexicoRoutes = new HashMap<String, String>();
        private int cntNumbersTotal = 0;
        private int shortestPrefix = 0;
        private int longestPrefix = 0;

        public CountryThread(String countryPrefix, String[] skipPrefixes, int maxPrefixLength,
                boolean includeInvalidNumbers) {
            this.countryPrefix = countryPrefix;
            this.skipPrefixes = skipPrefixes;
            this.maxPrefixLength = maxPrefixLength;
            this.includeInvalidNumbers = includeInvalidNumbers;
            if (this.countryPrefix.equalsIgnoreCase("52")) {
                loadMexicoRoutes();
            }
        }

        final private void loadMexicoRoutes() {
            try (BufferedReader br = new BufferedReader(new FileReader(getClass().getClassLoader().getResource("routes_mexico.csv").getFile()))) {
                String line = "";
                while ((line = br.readLine()) != null) {
                    String[] splits = line.split("\t");
                    if (splits[1].equalsIgnoreCase("mobile")) {
                        mexicoRoutes.put(splits[0], splits[1] + "_" + splits[2]);
                    } else {
                        mexicoRoutes.put(splits[0], splits[1]);
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        final private String getPhoneNumberType(PhoneNumber number) {
            PhoneNumberType numberType = phoneUtil.getNumberType(number);
            switch (numberType) {
                case FIXED_LINE:
                    return "landline";
                case MOBILE:
                    return "mobile";
                case TOLL_FREE:
                    return "freephone";
                case FIXED_LINE_OR_MOBILE:
                    return numberType.toString();
                case UNKNOWN:
                    log.warn("Unknown phone number type for " + number);
                    return null;
                default:
                    return "special";
            }
        }

        final private String createTag(PhoneNumber number) {
            String region = phoneUtil.getRegionCodeForNumber(number);
            CountryCode cc = CountryCode.getByCode(region);
            String country = null;
            if (cc != null) {
                country = cc.getName();
            } else {
                country = new Locale("", region).getDisplayCountry();
            }
            String longName = camelCase(country);
            
            try {
                if (Tags.valueOf(longName) != null) {
                    longName = Tags.valueOf(longName).tag;    
                }
            } catch (Exception e) {
                System.out.println("Exception at tag " + longName + " (" + number.getCountryCode() + number.getNationalNumber() + "): " + e.getMessage());
            }
            String type = getPhoneNumberType(number);
            if (type == null) {
                return null;
            }
            if (type.equalsIgnoreCase("FIXED_LINE_OR_MOBILE")) {
                type = "landline";
                if (countryPrefix.equalsIgnoreCase("52")) { // Lookup table for mexico
                    String prefix = ("" + number.getNationalNumber()).substring(0, 6);
                    String tagSuffix = mexicoRoutes.get(prefix);
                    if (tagSuffix != null) {
                        longName += "_" + tagSuffix;
                        return longName;
                    } else {
                        return "mexico_landline";
                    }
                }
            }
            longName += "_" + type;

            // Special cities
            if (number.getCountryCode() == 7) {
                if (number.getRawInput().substring(1,5).matches("7495|7496|7498|7499")) {
                    longName += "_" + "moscow";
                }
            } else if (number.getCountryCode() == 51) {
                if (number.getRawInput().substring(1,4).matches("511")) {
                    longName += "_" + "lima";
                }
            } else if (number.getCountryCode() == 56) {
                if (number.getRawInput().substring(1,4).matches("562")) {
                    longName += "_" + "santiago";
                }
            }
            
            if (type.equalsIgnoreCase("mobile")) {
                Locale geocodingLocale = new Locale("en", "US");
                String carrier = PhoneNumberToCarrierMapper.getInstance().getNameForNumber(number, geocodingLocale)
                        .toLowerCase();
                longName += "_" + (carrier.length() > 0 ? camelCase(carrier) : "other");
            }
            return longName;
        }

        final private RouteInfo getRoute(String prefix, String testNumber) {

            /*
            if (prefix.startsWith("7800")) {
                log.info(testNumber);
            }
            */

            if (testNumber == null || testNumber.length() == 0) {
                String padding = generatePadding(15-prefix.length());
                testNumber = "+" + prefix + padding;
            }
            
            PhoneNumber number = null;
            try {
                number = phoneUtil.parseAndKeepRawInput(testNumber, "us");
            } catch (NumberParseException e) {
                log.debug("invalid: " + testNumber + "(" + e + ")");
                return new RouteInfo("invalid_parseError");
            }

            boolean isValid = phoneUtil.truncateTooLongNumber(number);
            if (isValid) {
                // shorten by two digits and test if still valid (e.g. italy)
                PhoneNumber numberCopy = new PhoneNumber();
                numberCopy.mergeFrom(number);
                long natNum = number.getNationalNumber();
                natNum /= 100;
                numberCopy.setNationalNumber(natNum);
                if (phoneUtil.isValidNumber(numberCopy)) {
                    number = numberCopy;
                }
            } else {
                // TODO: manual truncate as phoneUtil based truncate does not work in every case (e.g. prefix 7800)
                if (phoneUtil.isPossibleNumberWithReason(number) != ValidationResult.TOO_SHORT &&
                    testNumber.length() > prefix.length() + 1) {
                        return getRoute(prefix, testNumber.substring(0,testNumber.length()-1));
                }
                log.debug("invalid: " + number.getRawInput());
                return new RouteInfo("invalid_tooShort");
            }
            String pnE146 = phoneUtil.format(number, PhoneNumberFormat.E164);
            if (!testNumber.startsWith(pnE146)){
                log.debug("invalid: " + number.getRawInput() + " (no match e164) ");
                return new RouteInfo("invalid_e164Mismatch");
            }

            String tag = createTag(number);
            if (tag == null) {
                log.debug("invalid: " + number.getRawInput() + " (invalid type) ");
                return new RouteInfo("invalid_typeInvalid");
            }
            
            RouteInfo route = new RouteInfo(tag);
            route.addPrefix(prefix);

            String example = pnE146.substring(1);
            int random = generateRandomNumber();
            if (randomValues.get(tag) == null) {
                randomValues.put(tag, new ArrayList<Integer>());
            };
            randomValues.get(tag).add(random);
            exampleNumbers.put(random,example);

            if (cntPerRoute.get(tag) == null) {
                cntPerRoute.put(tag, new AtomicInteger());
            }
            cntPerRoute.get(tag).addAndGet(1);
            cntNumbersTotal += 1;
            log.debug(tag + ": " + cntPerRoute.get(tag) + " / " + cntNumbersTotal + " = " + cntPerRoute.get(tag).floatValue()/cntNumbersTotal);
            
            /*
            if (prefix.startsWith("7800")) {
                log.info(tag);
            }
            */
            
            return route;
        }

        final private void emit(RouteInfo newRoute) {
            RouteInfo existingRoute = routes.get(newRoute.longTag);
            if (existingRoute != null) {
                existingRoute.merge(newRoute);
            } else {
                routes.put(newRoute.longTag, newRoute);
            }
        }

        final private RouteInfo emitRoutes(String prefix) {
            if (skipPrefixes != null && Arrays.stream(skipPrefixes).filter(v -> (prefix).startsWith(v)).count() > 0) {
                return null;
            }
            if (Long.valueOf(prefix) % 10000 == 0) {
                log.info(prefix);
            }
            HashMap<String, RouteInfo> routeNames = new HashMap<String, RouteInfo>();
            for (int i = 0; i < 10; i++) {
                String prefixSuffix = prefix + i;
                int prefixLength = prefixSuffix.length() - countryPrefix.length();
                RouteInfo newRoute = prefixLength < maxPrefixLength ? emitRoutes(prefixSuffix) : getRoute(prefixSuffix, null);
                if (newRoute != null) {
                    if (newRoute.isValid() || includeInvalidNumbers) {
                        RouteInfo existingRoute = routeNames.get(newRoute.longTag);
                        if (existingRoute == null) {
                            routeNames.put(newRoute.longTag,newRoute);
                            existingRoute = newRoute;
                        }
                        existingRoute.addPrefix(prefixSuffix);
                    }
                }
            }
            if (routeNames.isEmpty()) {
                return null;
            }
            List<String> keys = routeNames.entrySet().stream()
                    .sorted((e1,e2) -> {
                        if (e1.getKey().startsWith("invalid")) {
                            return 1;
                        } else if (e2.getKey().startsWith("invalid")) {
                            return -1;
                        } else {
                            return e1.getValue().compareTo(e2.getValue());
                        }
                    })
                    .map(e -> e.getKey()).collect(Collectors.toList());

            for (int i = 1; i < keys.size(); i++) {
                String key = keys.get(i);
                RouteInfo route = routeNames.get(key);
                emit(route);
            }

            RouteInfo topRoute = routeNames.get(keys.get(0));
            topRoute.clearPrefixes();
            /*
            if (topRoute.prefixes.size() > 5) {
                topRoute.clearPrefixes();
            }
            */
            if (prefix.equalsIgnoreCase(countryPrefix)) {
                topRoute.addPrefix(prefix);
                emit(topRoute);
            }
            return topRoute;
        }

        final public TreeMap<String, RouteInfo> call() {
            Instant start = Instant.now();
            log.info(countryPrefix + " start");
            emitRoutes(countryPrefix);
            Instant end = Instant.now();
            
            log.info(countryPrefix + " finished (" + Duration.between(start, end) + ")");
            for(Map.Entry<String,RouteInfo> entry : routes.entrySet()) {
                RouteInfo info = entry.getValue();
                if (info.isValid()) {
                    List<Integer> sortedRandom = randomValues.get(info.longTag).stream().sorted((e1,e2) -> {
                        if (e1 > e2) {
                            return -1;
                        }
                        if (e1 < e2) {
                            return 1;
                        }
                        return 0;
                    }).collect(Collectors.toList());
                    Integer key = sortedRandom.get(0);
                    info.exampleNumber = exampleNumbers.get(key);
                    //log.info( examples.get(0) + "..." + examples.get(1) + "..." +  examples.get(examples.size() - 1) + "..." +  examples.get(examples.size() - 1));
                    info.cntNumbers = cntPerRoute.get(info.longTag).get();
                    info.cntNumbersTotal = cntNumbersTotal;
                }
                log.info(info.longTag + " => " + info.prefixes.size() + " prefixes");
            }
            log.info("----------------------------------------------------------------");
            log.info("Phone numbers total: " + cntNumbersTotal);
            log.info("----------------------------------------------------------------");
            return routes;
        }
    }

    public static void printLandlineMap() {
        int[] prefixes = phoneUtil.getSupportedCallingCodes().stream().sorted().mapToInt(Integer::intValue).toArray();
        String[][] countries = new String[prefixes.length][2];
        for (int i = 0; i < prefixes.length; i++) {
            int prefix = prefixes[i];
            String region = phoneUtil.getRegionCodeForCountryCode(prefix);
            CountryCode cc = CountryCode.getByCode(region);
            String country = null;
            if (cc != null) {
                country = cc.getName();
            } else {
                country = new Locale("", region).getDisplayCountry();
            }
            String longName = RouteServlet.camelCase(country);
            if (Tags.valueOf(longName) != null) {
                longName = Tags.valueOf(longName).tag;
            }
            countries[i] = new String[]{""+prefix, longName};
        }
        System.out.println("{");
        for (int i = 0; i < countries.length; i++) {
            System.out.println("'"+countries[i][0]+"':'"+countries[i][1]+"_landline',");
        }
        System.out.println("}");
    }

    public static void _main(String[] args) throws Exception {

        RouteServlet s = new RouteServlet();
        TreeMap<String, RouteInfo> allRoutes = new TreeMap<String, RouteInfo>();
        
        RouteInfo info = s.new RouteInfo("test");
        allRoutes.put("test", info);
        createCSV(s, allRoutes);

        //printLandlineMap();
        //System.out.println(phoneUtil.getSupportedCallingCodes().stream().sorted().toArray());
        
        /*
        String pn = "+39320299996222";
        PhoneNumber n = phoneUtil.parseAndKeepRawInput(pn, "us");
        boolean wasTruncated = phoneUtil.truncateTooLongNumber(n);
        System.out.println(n.getNationalNumber());
        return;
        
        String pnE146 = phoneUtil.format(n, PhoneNumberFormat.E164);
        int padLen = pnE146.length() - "78009995".length() - 1;

        System.out.println(n);
        System.out.println(pnE146);
        System.out.println(wasTruncated);
        System.out.println(padLen);
        System.out.println(phoneUtil.getNumberType(n));
        */
    }

    public static void main(String[] args) throws Exception {
        Instant start = Instant.now();

        RouteServlet s = new RouteServlet();
        TreeMap<String, RouteInfo> allRoutes = new TreeMap<String, RouteInfo>();

        // Handle SIGINT
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                createCSV(s, allRoutes);
            }
        });

        int threadCount = 0;
        ExecutorService threadPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        ArrayList<Future> threads = new ArrayList<Future>();
        String [] prefixes2Use = args.length > 0 ? args : prefixes;
        for (int i = 0; i < prefixes2Use.length; i++) {
            String prefix = prefixes2Use[i];
            String[] skip = {};
            if (prefix.startsWith("1")) {
                skip = Arrays.stream(usSpecial).filter(v ->!v.equalsIgnoreCase(prefix)).toArray(String[]::new);
            } else if (prefix.equalsIgnoreCase("7")) {
                skip = kazakhstan;
            }
            CountryThread t = s.new CountryThread(prefix, skip, 7, false);
            threads.add(threadPool.submit(t));
            threadCount++;
        }

        // Wait for completion
        Iterator<Future> threadItr = threads.iterator();
        while (threadItr.hasNext()) {
            Future f = threadItr.next();
            try {
                TreeMap<String, RouteInfo> newRoutes = (TreeMap<String, RouteInfo>) f.get();
                List<String> keys = newRoutes.entrySet().stream().map(e -> e.getKey()).collect(Collectors.toList());
                for (int i = 0; i < keys.size(); i++) {
                    String key = keys.get(i);
                    RouteInfo newRoute = newRoutes.get(key);
                    RouteInfo existingRoute = allRoutes.get(key);
                    if (existingRoute != null) {
                        //System.out.println("merge existing routes for " + key);
                        existingRoute.merge(newRoute);
                    } else {
                        //System.out.println("add routes for " + key);
                        allRoutes.put(newRoute.longTag, newRoute);
                    }
                }
            } catch (Exception e) {
                System.err.println(e);
            }
            System.out.println(--threadCount + " threads remaining");
        }
        threadPool.shutdown();

        createCSV(s, allRoutes);
        Instant end = Instant.now();
        log.info("completed (" + Duration.between(start, end) + ")");
    }

    public static void createCSV(RouteServlet s, TreeMap<String, RouteInfo> routes) {
        System.out.println("create routes.txt");
        try {
            FileOutputStream os = new FileOutputStream(new File("routes.txt"));
            s.writeCSV(routes, os);
        } catch (Exception e) {
            System.err.println(e);
        }
    }
}