Instanz erstellen:
gcloud compute instances create routegen --image-family=debian-10 --image-project=debian-cloud --machine-type=n2d-standard-224 --scopes userinfo-email,cloud-platform --metadata-from-file startup-script=startup-script.sh --zone europe-west2-b --tags http-server
